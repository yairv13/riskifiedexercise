import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import indexRouter from "./server/routes/index.js";
import apiRouter from "./server/routes/api/api.js";
import { dirname } from "path";
import { fileURLToPath } from "url";
import cors from "cors";

import { config } from "dotenv";
config();

import * as R from "ramda";
global.R = R;

const app = express();
app.use(cors({ exposedHeaders: "Identifier" }));
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
const __dirname = dirname(fileURLToPath(import.meta.url));
app.use(express.static(path.join(__dirname, "../public")));
app.use("/", indexRouter);
app.use("/api", apiRouter);

export default app;
