import axios from "axios";
import promiseRetry from "promise-retry";

export const callCreditCardCompanyRetry = body =>
  promiseRetry(
    (retry, number) => {
      console.log(`attempt no. ${number}`); //leaving the log for your convenience :)
      return callCreditCardCompany(body).catch(err => {
        if (isBLError(err)) throw err;
        retry(err);
      });
    },
    {
      retries: 1
    }
  ).catch(err => {
    throw err;
  });

const callCreditCardCompany = ({ creditCardCompany, ...body }) => {
  const identifierHeader = { identifier: "Yair" };
  //totally separate calls, for func decoupling & flexibility in the future
  switch (creditCardCompany) {
    case "visa":
      return callVisa(body, identifierHeader);
    case "mastercard":
      return callMasterCard(body, identifierHeader);
    default:
      return;
  }
};

const callVisa = (
  { fullName, creditCardNumber, expirationDate, cvv, amount },
  identifierHeader
) =>
  axios({
    method: "POST",
    url:
      process.env.VISA_CHARGE_URL ||
      "https://interview.riskxint.com/visa/api/chargeCard",
    data: {
      fullName,
      number: creditCardNumber,
      expiration: expirationDate,
      cvv,
      totalAmount: amount
    },
    headers: identifierHeader
  });

const callMasterCard = (
  { fullName, creditCardNumber, expirationDate, cvv, amount },
  identifierHeader
) => {
  const splitName = R.split(" ", fullName);
  const length = splitName.length;

  const getFirstName = R.pipe(R.slice(0, length - 1), R.join(" "));
  const getLastName = R.pipe(R.slice(length - 1, length), R.head);

  const firstName = getFirstName(splitName);
  const lastName = getLastName(splitName);

  return axios({
    method: "POST",
    url:
      process.env.MASTERCARD_CHARGE_URL ||
      "https://interview.riskxint.com/mastercard/capture_card",
    data: {
      first_name: firstName,
      last_name: lastName,
      card_number: creditCardNumber,
      expiration: expirationDate,
      cvv,
      charge_amount: amount
    },
    headers: identifierHeader
  });
};

/*my understanding for what's a BL logic for this exercise - error has error/errors prop,
  for it's unclear how you define a BL logic in the document*/
export const isBLError = err => {
  const errData = err?.response?.data;
  if (!errData) return false;
  const hasError = R.has("error");
  const hasErrors = R.has("errors");
  return !(hasError(errData) || hasErrors(errData));
};

export const aggregateDecline = ({
  merchant,
  declinesPerMerchant,
  errData
}) => {
  const merchantDeclines = declinesPerMerchant[merchant];
  const getReason = R.pipe(R.values, R.head);
  const reason = getReason(errData);

  const reasonDocIndex = R.findIndex(
    reasonDoc => R.equals(reason, R.prop("reason", reasonDoc)),
    merchantDeclines || []
  );
  const reasonDoc = reasonDocIndex >= 0 && merchantDeclines[reasonDocIndex];

  const filterDocFromRest = R.remove(reasonDocIndex, 1);

  declinesPerMerchant[merchant] = reasonDoc
    ? [
        ...filterDocFromRest(merchantDeclines),
        { reason, count: R.prop("count", reasonDoc) + 1 }
      ]
    : R.append({ reason: reason, count: 1 }, merchantDeclines);
};
