import express from "express";
const router = express.Router();
import { createValidationFor, checkValidationResult } from "./validators.js";
import {
  callCreditCardCompanyRetry,
  isBLError,
  aggregateDecline
} from "./functions.js";

const declinesPerMerchant = {};

router.post(
  "/charge",
  createValidationFor("charge"),
  checkValidationResult,
  (req, res) => {
    callCreditCardCompanyRetry(req.body)
      .then(() => res.status(200).send())
      .catch(err => {
        if (isBLError(err)) {
          aggregateDecline({
            merchant: req.headers["merchant-identifier"],
            declinesPerMerchant,
            errData: err.response.data
          });
          res.status(200).send("Card declined");
        }
        const errCode = err?.response?.status;
        res.status(errCode || 500).send();
      });
  }
);

//assumption - without merchant-identifier throw error
router.get(
  "/chargeStatuses",
  createValidationFor("chargeStatuses"),
  checkValidationResult,
  (req, res) => {
    const merchant = req.headers["merchant-identifier"];
    res
      .status(200)
      .send(
        declinesPerMerchant[merchant] || `No decline records for ${merchant}`
      );
  }
);

export default router;
