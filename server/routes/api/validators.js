import { body, header, validationResult } from "express-validator";

export const createValidationFor = route => {
  switch (route) {
    case "charge":
      return [
        header("merchant-identifier").exists().isString(),
        body("fullName").exists().isString(),
        body("creditCardNumber").exists().isString(),
        body("creditCardCompany").exists().isString(),
        body("expirationDate").exists().isString(),
        body("cvv").exists().isString(),
        body("amount").exists().isDecimal()
      ];
    case "chargeStatuses":
      return header("merchant-identifier").exists().isString();

    default:
      return [];
  }
};

export const checkValidationResult = (req, res, next) => {
  const result = validationResult(req);
  if (result.isEmpty()) {
    return next();
  }
  res.status(400).send("BAD_REQUEST");
};
